import Layout from "../components/MyLayout.js";
import fetch from "isomorphic-unfetch";

const Index = props => (
  <Layout>
    <ul>
      {props.data.map(item => (
        <li key={item._id}>{item.name}</li>
      ))}
    </ul>
  </Layout>
);

Index.getInitialProps = async function() {
  const res = await fetch("https://<your-api-gateway-host>/dev/user");
  const data = await res.json();

  console.log(`Data fetched. Count: ${data.length}`);

  return {
    data: data.map(entry => entry.name)
  };
};

export default Index;
