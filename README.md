# SSR FETCH API DATA NEXT.JS

This example demonstrates how to fetch data from aws api gateway in next.js on server side

## Setup

```
npm install
node server.js
```

## Usage

In `index.js` update the `<your-api-gateway-host>` with your your api gateway host

## API setup

https://github.com/nsatija/aws-serverless-node
